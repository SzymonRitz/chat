/**
 *
 *  @author Ritz Szymon S12910
 *
 */

package zad1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;

public class Server {
	private ServerSocketChannel serverchannel;
	private Selector selector;

	public Server() {

		try {
			serverchannel = ServerSocketChannel.open();
			serverchannel.configureBlocking(false);
			serverchannel.socket().bind(new InetSocketAddress("localhost", 2345));
			selector = Selector.open();
			serverchannel.register(selector, SelectionKey.OP_ACCEPT);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}

		new ServerThread(selector, serverchannel).run();
	}

	public static void main(String[] args) {
		new Server();
	}
}