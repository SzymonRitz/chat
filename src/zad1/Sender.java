package zad1;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

public class Sender {

	public static void send(String text, SocketChannel sc) {
		text += "\n";
		try {
			ByteBuffer buff = Charset.forName("ISO-8859-2").encode(text);
			while (buff.hasRemaining()) {
				sc.write(buff);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String read(SocketChannel sc) {
		int number = 0;
		int readed;
		char sign;
		if (!sc.isOpen())
			return "";
		StringBuffer message = new StringBuffer();
		message.setLength(0);
		ByteBuffer buff = ByteBuffer.allocate(1024);
		buff.clear();
		try {
			while (true) {
				readed = sc.read(buff);
				if (readed > 0) {
					buff.flip();
					CharBuffer charBuffer = Charset.forName("ISO-8859-2").decode(buff);
					while (charBuffer.hasRemaining()) {
						sign = charBuffer.get();
						if (sign == '\r' || sign == '\n')
							break;
						message.append(sign);
					}
				} else {
					number++;
					if (number > 1000)
						break;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return message.toString();
	}

}