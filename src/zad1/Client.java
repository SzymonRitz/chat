/**
 *
 *  @author Ritz Szymon S12910
 *
 */

package zad1;

import java.awt.EventQueue;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Client {

	private SocketChannel serverChannel;
	private String name;

	public Client() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginGUI window = new LoginGUI();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Client(String name) {
		setName(name);
		String message;
		try {
			SocketChannel socketChannel = SocketChannel.open();
			socketChannel.connect(new InetSocketAddress("localhost", 2345));
			this.serverChannel = socketChannel;
		} catch (Exception ex) {
			JFrame frame = new JFrame("Sample frame");
			frame.setSize(400, 400);
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			JOptionPane.showMessageDialog(frame, "Błąd połączenia");
			System.exit(-1);
		}
		ChatGUI window = new ChatGUI(this);

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		while (true) {
			message = Sender.read(this.serverChannel);
			if (message.length() > 0) {
				window.addText(message);
			}
		}
	}

	public void sendMessage(String text) {
		Sender.send((getName() + " : " + text), this.serverChannel);
	}

	public String getName() {
		return name;
	}

	public SocketChannel getServerChannel() {
		return serverChannel;
	}

	public void setServerChannel(SocketChannel serverChannel) {
		this.serverChannel = serverChannel;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static void main(String[] args) {
		new Client();
	}
}
