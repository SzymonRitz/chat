package zad1;

import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

public class ServerThread extends Thread {

	private ServerSocketChannel serverchannel;
	private Selector selector;

	public ServerThread(Selector selector, ServerSocketChannel serverchannel) {
		// TODO Auto-generated constructor stub
		setSelector(selector);
		setServerchannel(serverchannel);
	}

	public void run() {
		while (true) {
			try {
				selector.select();
				Set keys = selector.selectedKeys();
				Iterator iterator = keys.iterator();
				while (iterator.hasNext()) {
					SelectionKey key = (SelectionKey) iterator.next();
					if (key.isAcceptable()) {
						SocketChannel socketChannel = serverchannel.accept();
						if (socketChannel != null) {
							socketChannel.configureBlocking(false);
							socketChannel.register(selector, (SelectionKey.OP_READ | SelectionKey.OP_WRITE));
						}
						continue;
					}

					if (key.isReadable()) {
						SocketChannel socketChannel = (SocketChannel) key.channel();
						String message = Sender.read(socketChannel);
						if (message.length() > 0) {
							message = "(" + new SimpleDateFormat("HH:mm").format(new Date()) + ") " + message;
							this.resendToAll(message);
						}
						continue;
					}
				}
			} catch (Exception exc) {
				exc.printStackTrace();
				continue;
			}
		}

	}

	private void resendToAll(String message) {
		try {
			selector.select();
			Set<SelectionKey> keys = selector.selectedKeys();
			Iterator<SelectionKey> iter = keys.iterator();
			while (iter.hasNext()) {
				SelectionKey key = (SelectionKey) iter.next();
				if (key.isWritable()) {
					SocketChannel cc = (SocketChannel) key.channel();
					Sender.send(message, cc);
				}
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	public Selector getSelector() {
		return selector;
	}

	public void setSelector(Selector selector) {
		this.selector = selector;
	}

	public ServerSocketChannel getServerchannel() {
		return serverchannel;
	}

	public void setServerchannel(ServerSocketChannel serverchannel) {
		this.serverchannel = serverchannel;
	}

}
