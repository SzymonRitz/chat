/**
 *
 *  @author Ritz Szymon S12910
 *
 */

package zad1;

public class Main {

	public static void main(String[] args) {

		Thread server = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				new Server();
			}
		});
		server.start();

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread client1 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				new Client("Szymon");
			}
		});
		client1.start();
		Thread client2 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				new Client("Łukasz");
			}
		});
		client2.start();
	}

}
