package zad1;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LoginGUI extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textField;

	public LoginGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.setBounds(100, 100, 356, 137);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		this.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("login");
		lblNewLabel.setBounds(7, 42, 46, 14);
		panel.add(lblNewLabel);

		textField = new JTextField();
		textField.setBounds(42, 39, 189, 20);
		panel.add(textField);
		textField.setColumns(10);

		JButton btnNewButton = new JButton("Zaloguj");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (textField.getText().length() > 3)
					new Client(textField.getText());
			}
		});
		btnNewButton.setBounds(241, 38, 89, 23);
		panel.add(btnNewButton);
	}

}